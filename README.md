# talk-cli
A command line interface for using Nextcloud Talk.

At the moment it is in very early stages. The user interface is not the most
polished yet and also the performance is quite bad. But simple reading and
writing of messages should already work.

## Getting Started
When first starting the application you will get prompted to enter your
Nextcloud server address to a config file.

After doing this you can execute `talk-cli -a' to grant talk-cli access to your
Nextcloud installation. Now you should have access to your channels and messages.

## Controls
| Key                 | Description          |
|---------------------|----------------------|
| <kbd>Tab</kbd>      | Switching channels   |
| <kbd>Ctrl + C</kbd> | Exit the application |

![Screenshot](./.assets/screenshot.png)