mod message;
pub use self::message::Message;

use crate::config;
use crate::dirs;

use config::Config;
use config::Credentials;
use reqwest::header::{ACCEPT, USER_AGENT};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::cmp::Reverse;
use std::collections::HashMap;
use std::fs;
use std::io::prelude::*;

pub struct Api {
    credentials: Credentials,
    config: Config,
}

impl Api {
    pub fn new() -> Api {
        Api {
            credentials: Credentials::new(),
            config: Config::new(),
        }
    }

    pub fn is_logged_in(&self) -> bool {
        let credentials = dirs::get_credentials();
        match credentials {
            Some(_x) => true,
            None => false,
        }
    }

    pub fn login(&self) {
        let authentication_json = self.get_authentication_json();
        self.print_login_message(authentication_json["login"].as_str().unwrap());
        self.poll_for_credentials(
            authentication_json["poll"]["endpoint"].as_str().unwrap(),
            authentication_json["poll"]["token"].as_str().unwrap(),
        );
    }

    pub fn get_messages(&self, token: &str, look_into_future: u64) -> Vec<Message> {
        let client = self.get_client(reqwest::Method::GET, &self.get_chat_url(token));
        let mut params = HashMap::new();
        let look_into_future = look_into_future.to_string();
        params.insert("lookIntoFuture", &look_into_future);
        let json_response: Value = client.json(&params).send().unwrap().json().unwrap();
        let data = json_response["ocs"]["data"].clone();
        serde_json::from_value(data).unwrap()
    }

    pub fn get_conversations(&self) -> Vec<Conversation> {
        let client = self.get_client(reqwest::Method::GET, &self.get_conversation_url());

        let json_response: Value = client.send().unwrap().json().unwrap();
        let data = json_response["ocs"]["data"].clone();
        let mut conversations: Vec<Conversation> = serde_json::from_value(data).unwrap();
        conversations.sort_by_key(|k| Reverse(k.last_ping));

        conversations
    }

    pub fn send_message(&self, message: String, chat_token: String) {
        let client = self.get_client(reqwest::Method::POST, &self.get_chat_url(&chat_token));
        let mut params = HashMap::new();
        params.insert("message", message);
        client.json(&params).send().unwrap();
    }

    fn get_client(&self, method: reqwest::Method, url: &str) -> reqwest::RequestBuilder {
        let client = reqwest::Client::new();
        client
            .request(method, url)
            .header(ACCEPT, "application/json")
            .header("OCS-APIREQUEST", "true")
            .basic_auth(
                self.credentials.login_name.clone(),
                Some(self.credentials.app_password.clone()),
            )
    }

    fn poll_for_credentials(&self, poll_url: &str, token: &str) {
        let mut body = HashMap::new();
        body.insert("token", token);
        let client = reqwest::Client::new();
        let mut response = client.post(poll_url).form(&body).send().unwrap();
        match response.status() {
            StatusCode::OK => {
                fs::write(dirs::create_credentials(), response.text().unwrap())
                    .expect("Unable to write file");
            }
            status => {
                println!("Received status code: {:?}", status);
                std::process::exit(1);
            }
        }
    }

    fn print_login_message(&self, login_url: &str) {
        println!(
            r#"To use this application you need to authorize it with your Nextcloud account.
Please open the following URL to grant access:
    {}

    After granting access please press enter to continue"#,
            login_url
        );
        let _ = std::io::stdin().read(&mut [0u8]).unwrap();
    }

    fn get_authentication_json(&self) -> Value {
        let client = reqwest::Client::new();
        let response = client
            .post(&self.get_login_url())
            .header(USER_AGENT, "talk_cli")
            .header("OCS-APIREQUEST", "true")
            .send();
        response.unwrap().json().unwrap()
    }

    fn get_login_url(&self) -> String {
        self.get_base_url() + "/index.php/login/v2"
    }

    fn get_conversation_url(&self) -> String {
        self.get_base_url() + "/ocs/v2.php/apps/spreed/api/v1/room"
    }

    fn get_chat_url(&self, token: &str) -> String {
        self.get_base_url() + "/ocs/v2.php/apps/spreed/api/v1/chat/" + token
    }

    fn get_base_url(&self) -> String {
        self.config.server.clone()
    }
}

impl Default for Api {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Conversation {
    token: String,
    name: String,
    display_name: String,
    last_ping: u64,
}

impl Conversation {
    pub fn token(&self) -> String {
        self.token.clone()
    }
}

impl AsRef<str> for Conversation {
    fn as_ref(&self) -> &str {
        &self.display_name
    }
}
