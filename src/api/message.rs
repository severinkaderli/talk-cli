use chrono::serde::ts_seconds;
use chrono::{DateTime, Local, Utc};
use serde::de::Deserializer;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// Represents a single message from the Nextcloud Talk Chat API.
///
/// For more information look at the official API
/// [documentation](https://nextcloud-talk.readthedocs.io/en/latest/chat/#receive-chat-messages-of-a-conversation).
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Message {
    /// ID of the message
    id: u64,
    /// Timestamp in seconds in the UTC time zone
    #[serde(with = "ts_seconds")]
    timestamp: DateTime<Utc>,
    /// Display name of the message author
    actor_display_name: String,
    /// Message string with placeholders,
    message: String,
    /// Message parameters for [`message`](message)
    ///
    /// The API either returns an empty array or an object of objects. That's
    /// why I need a custom deserialize function here.
    #[serde(deserialize_with = "to_hashmap")]
    message_parameters: HashMap<String, Parameter>,
}

impl Message {
    pub fn id(&self) -> u64 {
        self.id
    }

    /// Return the local timestamp of the message
    pub fn timestamp(&self) -> DateTime<Local> {
        self.timestamp.with_timezone(&Local)
    }

    pub fn author(&self) -> String {
        self.actor_display_name.clone()
    }

    pub fn message(&self) -> String {
        let mut message = self.message.clone();
        for (key, parameter) in &self.message_parameters {
            message = message.replace(&format!("{{{}}}", key), &parameter.name);
        }

        message
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct Parameter {
    r#type: String,
    id: String,
    name: String,
}

/// Deserializer to convert all types to an empty HashMap unless they are
/// a HashMap.
fn to_hashmap<'de, D>(deserializer: D) -> Result<HashMap<String, Parameter>, D::Error>
where
    D: Deserializer<'de>,
{
    let mut map: HashMap<String, Parameter> = HashMap::new();

    // If the value is a map use its value.
    let deserialized_value = HashMap::deserialize(deserializer);
    match deserialized_value {
        Err(_error) => (),
        Ok(value) => map = value,
    }

    Ok(map)
}
