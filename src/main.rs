extern crate chrono;
#[macro_use]
extern crate clap;
extern crate reqwest;
extern crate serde_json;
extern crate termion;
extern crate toml;
extern crate tui;
extern crate xdg;

pub mod api;
pub mod application;
pub mod config;
pub mod dirs;
pub mod event;

use api::Api;
use application::Application;

fn main() {
    let matches = application::get_cli().get_matches();
    let api = Api::new();

    if matches.is_present("authenticate") {
        api.login();
    }

    if !api.is_logged_in() {
        println!("No credentials found. Please authenticate yourself using 'talk-cli -a'");
        std::process::exit(0);
    }

    let mut app = Application::new(api);
    app.start();
}
