use serde::{Deserialize, Serialize};
mod credentials;

pub use self::credentials::Credentials;

use crate::dirs;

use std::fs;

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Config {
    pub server: String,
}

impl Config {
    pub fn new() -> Config {
        if dirs::get_config().is_none() {
            fs::write(
                dirs::create_config(),
                toml::to_string(&Config::default()).unwrap(),
            )
            .expect("Unable to write file");
        }

        let config_path = dirs::get_config().unwrap();
        let toml = fs::read_to_string(&config_path).unwrap();
        let config: Config = toml::from_str(&toml).unwrap();

        if config.server.is_empty() {
            println!(
                "Please enter your server address in {}",
                &config_path.to_str().unwrap()
            );
            std::process::exit(0);
        }

        config
    }
}
