use serde::{Deserialize, Serialize};

use crate::dirs;

use std::fs;

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Credentials {
    pub server: String,
    pub login_name: String,
    pub app_password: String,
}

impl Credentials {
    pub fn new() -> Credentials {
        let file_name = dirs::get_credentials();
        match file_name {
            Some(path) => {
                let json = fs::read_to_string(path).unwrap();
                serde_json::from_str(&json).unwrap()
            }
            None => Credentials {
                server: String::new(),
                login_name: String::new(),
                app_password: String::new(),
            },
        }
    }
}
