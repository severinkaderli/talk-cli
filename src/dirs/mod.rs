use std::path::PathBuf;

pub fn get_credentials() -> Option<PathBuf> {
    get_base_dir().find_cache_file("credentials.json")
}

pub fn create_credentials() -> PathBuf {
    get_base_dir().place_cache_file("credentials.json").unwrap()
}

pub fn get_config() -> Option<PathBuf> {
    get_base_dir().find_config_file("config.toml")
}

pub fn create_config() -> PathBuf {
    get_base_dir().place_config_file("config.toml").unwrap()
}

fn get_base_dir() -> xdg::BaseDirectories {
    xdg::BaseDirectories::with_prefix("talk-cli").unwrap()
}
