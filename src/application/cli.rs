use clap::{App, Arg};

pub fn get_cli() -> App<'static, 'static> {
    App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("authenticate")
                .short("a")
                .long("authenticate")
                .help("Requests authentication to a Nextcloud server."),
        )
}
