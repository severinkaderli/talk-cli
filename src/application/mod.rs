mod cli;
mod gui;

pub use self::cli::get_cli;
pub use self::gui::Gui;

use crate::api;
use crate::event;

use api::{Api, Conversation, Message};
use chrono::Utc;
use event::{Event, Events};
use termion::event::Key;

pub struct Application {
    channels: Vec<Conversation>,
    messages: Vec<Message>,
    current_channel: Option<usize>,
    gui: Gui,
    events: Events,
    last_fetch_time: i64,
    api: Api,
}

impl Application {
    pub fn new(api: Api) -> Application {
        Application {
            channels: Vec::new(),
            messages: Vec::new(),
            current_channel: Some(0),
            gui: Gui::new(),
            events: Events::new(),
            last_fetch_time: 0,
            api,
        }
    }

    fn get_conversations(&mut self) {
        self.channels = self.api.get_conversations();
    }

    fn get_messages(&mut self, last_known_message_id: u64) {
        self.messages = self
            .api
            .get_messages(&self.channel().token(), last_known_message_id);
        self.last_fetch_time = Utc::now().timestamp();
    }

    pub fn draw(&mut self) {
        self.gui
            .draw(self.current_channel(), self.channels(), self.messages());
    }

    pub fn channels(&self) -> Vec<Conversation> {
        self.channels.clone()
    }

    pub fn messages(&self) -> Vec<Message> {
        self.messages.clone()
    }

    pub fn channel(&self) -> Conversation {
        self.channels()
            .get(self.current_channel().unwrap())
            .unwrap()
            .clone()
    }

    pub fn current_channel(&self) -> Option<usize> {
        self.current_channel
    }

    pub fn start(&mut self) {
        self.get_conversations();

        loop {
            let current_timestamp = Utc::now().timestamp();
            if current_timestamp > (self.last_fetch_time + 5) {
                self.get_messages(0);
            }

            self.draw();

            if self.handle_input() {
                break;
            }
        }
    }

    pub fn handle_input(&mut self) -> bool {
        match self.events.next().unwrap() {
            Event::Input(input) => match input {
                Key::Char('\n') => {
                    self.api
                        .send_message(self.gui.input(), self.channel().token());
                    self.gui.set_input(String::new());
                    self.get_messages(1);
                    //self.messages.(new_message);
                }
                Key::Char('\t') => {
                    self.current_channel = if let Some(channel_index) = self.current_channel {
                        if channel_index >= self.channels().len() - 1 {
                            Some(0)
                        } else {
                            Some(channel_index + 1)
                        }
                    } else {
                        Some(0)
                    };
                    self.get_messages(0);
                }
                Key::Char(c) => {
                    self.gui.input_push(c);
                }
                Key::Backspace => {
                    self.gui.input_pop();
                }
                Key::Up => {
                    if self.gui.scroll() > 0 {
                        self.gui.decrease_scroll(1);
                    }
                }
                Key::PageUp => {
                    if self.gui.scroll() < 10 {
                        self.gui.set_scroll(0);
                    } else {
                        self.gui.decrease_scroll(10);
                    }
                }
                Key::Down => {
                    self.gui.increase_scroll(1);
                }
                Key::PageDown => {
                    self.gui.increase_scroll(10);
                }
                Key::Ctrl('c') => return true,
                _ => {}
            },
            Event::Tick => {}
        }

        false
    }
}
