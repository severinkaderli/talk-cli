use crate::api;

use api::{Conversation, Message};

use std::io::{self, Write};
use termion::cursor::Goto;
use termion::raw::{IntoRawMode, RawTerminal};
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, Paragraph, SelectableList, Text, Widget};
use tui::Terminal;
use unicode_width::UnicodeWidthStr;

pub struct Gui {
    scroll: u32,
    terminal: Terminal<TermionBackend<AlternateScreen<RawTerminal<io::Stdout>>>>,
    input: String,
}

impl Gui {
    pub fn new() -> Gui {
        let stdout = io::stdout().into_raw_mode().unwrap();
        let stdout = AlternateScreen::from(stdout);
        let backend = TermionBackend::new(stdout);
        Gui {
            scroll: 0,
            terminal: Terminal::new(backend).unwrap(),
            input: String::new(),
        }
    }

    pub fn increase_scroll(&mut self, lines: u32) {
        self.scroll += lines;
    }

    pub fn decrease_scroll(&mut self, lines: u32) {
        self.scroll -= lines;
    }

    pub fn set_scroll(&mut self, line: u32) {
        self.scroll = line;
    }

    pub fn input_push(&mut self, c: char) {
        self.input.push(c);
    }

    pub fn input(&self) -> String {
        self.input.clone()
    }

    pub fn set_input(&mut self, input: String) {
        self.input = input;
    }

    pub fn input_pop(&mut self) {
        self.input.pop();
    }
    fn get_messages_as_text(&self, messages: Vec<Message>) -> Vec<Text<'static>> {
        let mut text = Vec::new();
        for message in messages.iter() {
            text.push(Text::styled(
                format!(
                    "{} – {}\n",
                    message.timestamp().format("%F %T"),
                    message.author()
                ),
                Style::default().modifier(Modifier::BOLD),
            ));
            text.push(Text::raw(format!("{}\n\n", message.message())));
        }

        text
    }

    pub fn scroll(&self) -> u32 {
        self.scroll
    }

    pub fn draw(
        &mut self,
        current_channel: Option<usize>,
        channels: Vec<Conversation>,
        messages: Vec<Message>,
    ) {
        let mut cursor_x_position = 1;
        let mut cursor_y_position = 1;
        let input = self.input.clone();
        let text_messages = self.get_messages_as_text(messages);
        let scroll = self.scroll();

        self.terminal
            .draw(|mut f| {
                let chunks = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints([Constraint::Percentage(25), Constraint::Percentage(75)].as_ref())
                    .split(f.size());
                SelectableList::default()
                    .block(Block::default().borders(Borders::ALL).title("Channels"))
                    .items(&channels)
                    .select(current_channel)
                    .highlight_symbol(">")
                    .render(&mut f, chunks[0]);
                {
                    let chunks = Layout::default()
                        .direction(Direction::Vertical)
                        .constraints([Constraint::Min(3), Constraint::Length(3)].as_ref())
                        .split(chunks[1]);
                    Paragraph::new(text_messages.iter())
                        .block(Block::default().borders(Borders::ALL).title("Messages"))
                        .wrap(true)
                        .scroll(scroll as u16)
                        .render(&mut f, chunks[0]);
                    Paragraph::new([Text::raw(input)].iter())
                        .style(Style::default().fg(Color::Yellow))
                        .block(Block::default().borders(Borders::ALL).title("Input"))
                        .render(&mut f, chunks[1]);

                    cursor_x_position = chunks[1].x + 2;
                    cursor_y_position = chunks[1].y + 2;
                }
            })
            .unwrap();

        // Put the cursor back inside the input box
        write!(
            self.terminal.backend_mut(),
            "{}",
            Goto(
                cursor_x_position + self.input.width() as u16,
                cursor_y_position
            )
        )
        .unwrap();
        io::stdout().flush().ok();
    }
}

impl Default for Gui {
    fn default() -> Self {
        Self::new()
    }
}
